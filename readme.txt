link git : https://gitlab.com/lehaiduongbmt2/machine_learning_vinbigdata.git
link model: https://drive.google.com/file/d/1HbBlhSZOlJNQ_uveVP0xoD7Xx2v4CyOJ/view?usp=sharing

run code 
# install requirements
pip install -r requirements.txt

# run server interminal 1
python main_fastapi.py

# run font end in terminal 2
python font_end.py

streamlit run font_end.py
